package io.osimis.tn;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static io.osimis.tn.ConnectivityTester.testConnectivity;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

public class MultipleServers {
    @Rule
    public WireMockRule foo = new WireMockRule(options().dynamicPort());

    @Rule
    public WireMockRule bar = new WireMockRule(options().dynamicPort());

    private TestResourceRetriever fooRetriever;
    private TestResourceRetriever barRetriever;

    @Before
    public void setUp() {
        fooRetriever = new TestResourceRetriever(foo.port());
        barRetriever = new TestResourceRetriever(bar.port());
    }

    @Test
    public void serversShouldOpenTheirPort() throws IOException {
        foo.stubFor(get("/").willReturn(aResponse()));
        bar.stubFor(get("/").willReturn(aResponse()));
        testConnectivity(foo.port());
        testConnectivity(bar.port());
    }

    @Test
    public void serversShouldSendAResponse() throws IOException {
        stubResource(foo, "hello world");
        stubResource(bar, "hello world");
        String fooMessage = fooRetriever.retrieve();
        String barMessage = barRetriever.retrieve();
        assertThat(fooMessage, containsString("hello"));
        assertThat(barMessage, containsString("hello"));
    }

    private void stubResource(WireMockRule server, String message) {
        server.stubFor(get("/").willReturn(aResponse()
                .withHeader("Content-Type", "text/plain")
                .withBody(message)));
    }
}


