package io.osimis.tn;

import java.io.IOException;
import java.net.Socket;

import static java.net.InetAddress.getLocalHost;

public class ConnectivityTester {
    public static void testConnectivity(int port) throws IOException {
        try (Socket ignored = new Socket(getLocalHost(), port)) {}
    }
}
