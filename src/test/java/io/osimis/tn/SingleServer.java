package io.osimis.tn;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static io.osimis.tn.ConnectivityTester.testConnectivity;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

public class SingleServer {
    @Rule
    public WireMockRule server = new WireMockRule(options().dynamicPort());

    private TestResourceRetriever retriever;

    @Before
    public void setUp() {
        retriever = new TestResourceRetriever(server.port());
    }

    @Test
    public void itShouldOpenItsPort() throws IOException {
        stubFor(get("/").willReturn(aResponse()));
        testConnectivity(server.port());
    }

    @Test
    public void itShouldSendAResponse() throws IOException {
        stubFor(get("/").willReturn(aResponse()
                .withHeader("Content-Type", "text/plain")
                .withBody("hello world")));
        String content = retriever.retrieve();
        assertThat(content, containsString("hello"));
    }
}
