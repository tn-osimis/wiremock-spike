package io.osimis.tn;

import org.apache.commons.io.IOUtils;
import sun.net.www.content.text.PlainTextInputStream;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.nio.charset.Charset.defaultCharset;

public final class TestResourceRetriever {

    private int port;

    public TestResourceRetriever(int port) {
        this.port = port;
    }

    public String retrieve() throws IOException {
        URL url = new URL("http", "localhost", port, "/");
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.connect();
        return IOUtils.toString((PlainTextInputStream)conn.getContent(), defaultCharset());
    }
}
